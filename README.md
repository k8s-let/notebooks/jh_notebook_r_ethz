# Standard Jupyter Notebook for LET JupyterHub

Based on the [r-notebook](https://hub.docker.com/r/jupyter/r-notebook) from Jupyter. 

## Branches
Currently (Aug. 2022):

 - the *master* branch is used for the image compatible with Helm chart 1.x.
 - the *hub-2* branch is used for the image compatible with Helm chart 2.x.
 
 Tags for Helm chart v1.x are named *1.x*, those for Helm chart 2.x (surprise) are named *2.x*. Images can be pulled by tag regardless of branch.
 
