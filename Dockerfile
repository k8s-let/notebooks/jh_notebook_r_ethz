FROM jupyter/r-notebook:hub-1.4.2

USER root

# Install ETHZ root ca certificates
ADD http://pkiaia.ethz.ch/aia/ETHZRootCA2020.pem /usr/local/share/ca-certificates/ETHZRootCA2020.crt
ADD http://pkiaia.ethz.ch/aia/ETHZIssuingCA2020.pem /usr/local/share/ca-certificates/ETHZIssuingCA2020.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalRootCA.pem /usr/local/share/ca-certificates/DigiCertGlobalRootCA.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertTLSRSASHA2562020CA1-1.pem /usr/local/share/ca-certificates/DigiCertTLSRSASHA2562020CA1-1.crt
RUN chmod 644 /usr/local/share/ca-certificates/* && update-ca-certificates

RUN apt-get update && apt-get upgrade -y && apt-get install -y zip

RUN conda update -n base conda && conda install --quiet --yes \
  nbgitpuller \
  r-ape \
  r-bio3d \
  r-devtools \
  r-lpsolve \
  r-lhs \
  r-rsolnp \
  r-terra \
  && \
  conda install --quiet --yes -c bioconda bioconductor-all bioconductor-deseq2 bioconductor-tximport bioconductor-tximeta \
  && \
  conda clean --all -f -y

RUN Rscript -e "install.packages(pkgs=c('sensitivity'), repos=c('http://cran.r-project.org'), dependencies=TRUE, timeout=300)" \
  && \
  Rscript -e "devtools::install_github(repo = 'AECP-ETHZ/ETH.OLP')"

# Fix timezone
RUN ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime && echo "Europe/Zurich" >> /etc/timezone

USER 1000
